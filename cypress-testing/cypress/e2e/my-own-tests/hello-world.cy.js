/// <reference types="cypress" />

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImF6a2FudXJodWRhIiwiX2lkIjoiNjMzNDQ4YmZhZTliZGMwMDA5MDI5YTY2IiwibmFtZSI6ImF6a2EgbnVyaHVkYSIsImlhdCI6MTY2NDM3MDg4MCwiZXhwIjoxNjY5NTU0ODgwfQ.Yxo2X7Jc7baAav-TZ3yM_Mo0ZlKfLz2ZOmkLaHSRiWw'

describe('Basic Tests', () => {
  it('Every basic element exists', () => {
    cy.viewport(1280, 720)
    cy.visit('https://codedamn.com')

    // mocha
    cy.contains('Explore All Roadmaps')

    cy.get('div#root').should('exist')
    cy.get('[data-testid=logo]').click()
  })

  it('Every basic element exists on mobile', () => {
    cy.viewport('iphone-x')
    cy.visit('https://codedamn.com')
  })

  it('Login page looks good', () => {
    cy.viewport(1280, 720)
    cy.visit('https://codedamn.com')

    cy.contains('Sign in').click()
    cy.contains('Sign in to codedamn').should('exist')
    cy.contains('Create Free Account').should('exist')
    cy.contains('Forgot your password?').should('exist')
    cy.get('[data-testid=username]').should('exist')
    cy.get('[data-testid=google-oauth-btn]').should('exist')
    cy.get('[data-testid=github-oauth-btn]').should('exist')
    cy.contains('Don\'t have an account').should('exist')
  })

  it('The login page links work', () => {
    cy.viewport(1280, 720)
    cy.visit('https://codedamn.com')

    // 1. Sign in page
    cy.contains('Sign in').click()

    // 2. Reset password page
    cy.contains('Forgot your password?').click({ force: true })

    // 3. Verify your page URL
    cy.url().should('include', '/password-reset')

    // 4. Go back, on the sign in page
    cy.go('back')

    cy.contains('Create Free Account').click()
    cy.url().should('include', '/register')
  })

  it('Login should work fine', () => {
    cy.viewport(1280, 720)
    cy.visit('https://codedamn.com')

    cy.contains('Sign in').click()
    cy.contains('Sign in to codedamn').should('exist')

    cy.get('[data-testid=username]').type('azkanurhuda', { force: true })
    cy.get('[data-testid=password]').type('codedamnazka20', { force: true })

    cy.get('[data-testid=login]').click({ force: true })
  })

})

