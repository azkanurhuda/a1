/// <reference types="cypress" />

const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImF6a2FudXJodWRhIiwiX2lkIjoiNjMzNDQ4YmZhZTliZGMwMDA5MDI5YTY2IiwibmFtZSI6ImF6a2EgbnVyaHVkYSIsImlhdCI6MTY2NDM3MDg4MCwiZXhwIjoxNjY5NTU0ODgwfQ.Yxo2X7Jc7baAav-TZ3yM_Mo0ZlKfLz2ZOmkLaHSRiWw"

describe('Basic Authenticated Desktop Tests', () => {
  before(() => {
    cy.then(() => {
      window.localStorage.setItem('__auth__token', token)
    })
  })

  beforeEach(() => {
    cy.viewport(1280, 720)
    cy.visit('https://codedamn.com')
  })

  it('Should pass', () => {
    cy.visit('https://codedamn.com/playground/8W9S2QwkUASdKuDO_XHB2')

    cy.log('Checking for sidebar')

    cy.log('Checking bottom left button')

    cy.log('Playground is initializing')
  })
})
